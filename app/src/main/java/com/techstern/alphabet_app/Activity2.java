package com.techstern.alphabet_app;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;
import java.util.Random;

import static android.R.attr.name;

public class Activity2 extends AppCompatActivity implements Activity2adapter.ItemClickCallBack {
    private ArrayList listData;

    private RecyclerView recyclerView; //recyclerobject

    private Activity2adapter aAdapter; // adapterobject

    LinearLayoutManager layoutManager;
    TextView  fontt;
    PopupWindow popup;
    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();

        listData = (ArrayList) Seeddata2.getListData();
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_activity_2);
        aAdapter = new Activity2adapter(Seeddata2.getListData());
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this) {
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }
            public boolean  canScrollVertically() {
                return false;
            }
        };
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(aAdapter);
        aAdapter.setItemClickCallBack(this);
    }

    @Override
    public void option1Onclick(View v, int p) {
        final PopupWindow popup;
        Activity2model item = (Activity2model) listData.get(p);
        String option1 = item.getOption1();
        String answer = item.getAnswer();
        String answercheck = item.getAnswercheck();
        final int check2 = item.getCheck2();
        String question1 = item.getQuestion1();
        String question2 = item.getQuestion2();
        String question1visibility = item.getQuestion1visibility();
        String question2visibility = item.getQuestion2visibility();
        String answervisibility = item.getAnswervisibility();



        Random r = new Random();
        int adv = r.nextInt(10 - 1) + 1;

        if(adv == 8 || adv == 5 || adv == 3){
            InterstitialAd mInterstitialAd;
            final InterstitialAd interstitial;

            AdRequest adRequest = new AdRequest.Builder().build();

            // Prepare the Interstitial Ad
            interstitial = new InterstitialAd(Activity2.this);
// Insert the Ad Unit ID
            interstitial.setAdUnitId(getString(R.string.admob_interstitial_id));

            interstitial.loadAd(adRequest);
// Prepare an Interstitial Ad Listener
            interstitial.setAdListener(new AdListener() {
                public void onAdLoaded() {
// Call displayInterstitial() function
                    // If Ads are loaded, show Interstitial else show nothing.
                    if (interstitial.isLoaded()) {
                        interstitial.show();
                    }
                }
            });
        }

        if (option1 == answercheck){
            LayoutInflater li = (LayoutInflater)getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
            View layout = li.inflate(R.layout.custom_toast2, (ViewGroup) findViewById(R.id.custom_toast2));

            View tview1 =  layout.findViewById(R.id.tview1);
            View tview2 =  layout.findViewById(R.id.tview2);
            View tview3 =  layout.findViewById(R.id.tview3);
            tview1.setVisibility(View.INVISIBLE);
            tview2.setVisibility(View.INVISIBLE);
            tview3.setVisibility(View.INVISIBLE);

            ImageView ivrighticon1 = (ImageView) layout.findViewById(R.id.righticon1);
            ImageView ivrighticon2 = (ImageView) layout.findViewById(R.id.righticon2);
            ImageView ivrighticon3 = (ImageView) layout.findViewById(R.id.righticon3);

            ivrighticon1.setVisibility(View.INVISIBLE);
            ivrighticon2.setVisibility(View.INVISIBLE);
            ivrighticon3.setVisibility(View.INVISIBLE);

            if (question1visibility == "v" && question2visibility == "v" && answervisibility == "i"){
                tview2.setVisibility(View.VISIBLE);
                ivrighticon2.setVisibility(View.VISIBLE);
            }
            else if (question1visibility == "v" && question2visibility == "i" && answervisibility == "v"){
                tview3.setVisibility(View.VISIBLE);
                ivrighticon3.setVisibility(View.VISIBLE);
            }
            else if (question1visibility == "i" && question2visibility == "v" && answervisibility == "v"){
                tview1.setVisibility(View.VISIBLE);
                ivrighticon1.setVisibility(View.VISIBLE);
            }

            TextView t_question1 = (TextView) layout.findViewById(R.id.toast_question1);
            Typeface font = Typeface.createFromAsset(getAssets(), "fonts/custom.TTF");
            t_question1.setTypeface(font);

            t_question1.setText(question1);

            TextView t_question2 = (TextView) layout.findViewById(R.id.toast_question2);
            t_question2.setTypeface(font);
            t_question2.setText(question2);

            TextView t_answer = (TextView) layout.findViewById(R.id.toast_answer);
            t_answer.setTypeface(font);
            t_answer.setText(answer);

            popup = new PopupWindow(layout, RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.MATCH_PARENT,true);
            popup.setAnimationStyle(R.style.Animation);
            popup.showAtLocation(layout, Gravity.CENTER, 0, 0);


            new CountDownTimer(2000, 1000) {

                @Override
                public void onTick(long millisUntilFinished) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onFinish() {
                    // TODO Auto-generated method stub

                    popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                        @Override
                        public void onDismiss() {
                            popup.setAnimationStyle(R.style.Animation);
                        }
                    });
                    popup.dismiss();
                    recyclerView.scrollToPosition(check2);

                }
            }.start();


        }else{
            LayoutInflater li = getLayoutInflater();
            View layout = li.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.customtos));
            Toast toast = new Toast(getApplicationContext());
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setView(layout);//setting the view of custom toast layout
            toast.show();
        }
//        aAdapter.setListData(listData);
//        aAdapter.notifyDataSetChanged();
    }

    @Override
    public void option2Onclick(View v, int p) {
        final PopupWindow popup;
        Activity2model item = (Activity2model) listData.get(p);
        String option2 = item.getOption2();
        String answer = item.getAnswer();
        String answercheck = item.getAnswercheck();
        final int check2 = item.getCheck2();
        String question1 = item.getQuestion1();
        String question2 = item.getQuestion2();
        String question1visibility = item.getQuestion1visibility();
        String question2visibility = item.getQuestion2visibility();
        String answervisibility = item.getAnswervisibility();


        Random r = new Random();
        int adv = r.nextInt(10 - 1) + 1;

        if(adv == 8 || adv == 5 || adv == 3){
            InterstitialAd mInterstitialAd;
            final InterstitialAd interstitial;

            AdRequest adRequest = new AdRequest.Builder().build();

            // Prepare the Interstitial Ad
            interstitial = new InterstitialAd(Activity2.this);
// Insert the Ad Unit ID
            interstitial.setAdUnitId(getString(R.string.admob_interstitial_id));

            interstitial.loadAd(adRequest);
// Prepare an Interstitial Ad Listener
            interstitial.setAdListener(new AdListener() {
                public void onAdLoaded() {
// Call displayInterstitial() function
                    // If Ads are loaded, show Interstitial else show nothing.
                    if (interstitial.isLoaded()) {
                        interstitial.show();
                    }
                }
            });
        }


        if (option2 == answercheck){
            LayoutInflater li = (LayoutInflater)getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
            View layout = li.inflate(R.layout.custom_toast2, (ViewGroup) findViewById(R.id.custom_toast2));


            View tview1 =  layout.findViewById(R.id.tview1);
            View tview2 =  layout.findViewById(R.id.tview2);
            View tview3 =  layout.findViewById(R.id.tview3);
            tview1.setVisibility(View.INVISIBLE);
            tview2.setVisibility(View.INVISIBLE);
            tview3.setVisibility(View.INVISIBLE);

            ImageView ivrighticon1 = (ImageView) layout.findViewById(R.id.righticon1);
            ImageView ivrighticon2 = (ImageView) layout.findViewById(R.id.righticon2);
            ImageView ivrighticon3 = (ImageView) layout.findViewById(R.id.righticon3);

            ivrighticon1.setVisibility(View.INVISIBLE);
            ivrighticon2.setVisibility(View.INVISIBLE);
            ivrighticon3.setVisibility(View.INVISIBLE);

            if (question1visibility == "v" && question2visibility == "v" && answervisibility == "i"){
                tview2.setVisibility(View.VISIBLE);
                ivrighticon2.setVisibility(View.VISIBLE);
            }
            else if (question1visibility == "v" && question2visibility == "i" && answervisibility == "v"){
                tview3.setVisibility(View.VISIBLE);
                ivrighticon3.setVisibility(View.VISIBLE);
            }
            else if (question1visibility == "i" && question2visibility == "v" && answervisibility == "v"){
                tview1.setVisibility(View.VISIBLE);
                ivrighticon1.setVisibility(View.VISIBLE);
            }


            TextView t_question1 = (TextView) layout.findViewById(R.id.toast_question1);
            Typeface font = Typeface.createFromAsset(getAssets(), "fonts/custom.TTF");
            t_question1.setTypeface(font);

            t_question1.setText(question1);

            TextView t_question2 = (TextView) layout.findViewById(R.id.toast_question2);
            t_question2.setTypeface(font);
            t_question2.setText(question2);

            TextView t_answer = (TextView) layout.findViewById(R.id.toast_answer);
            t_answer.setTypeface(font);
            t_answer.setText(answer);

            popup = new PopupWindow(layout, RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.MATCH_PARENT,true);
            popup.setAnimationStyle(R.style.Animation);
            popup.showAtLocation(layout, Gravity.CENTER, 0, 0);


            new CountDownTimer(2000, 1000) {

                @Override
                public void onTick(long millisUntilFinished) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onFinish() {
                    // TODO Auto-generated method stub

                    popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                        @Override
                        public void onDismiss() {
                            popup.setAnimationStyle(R.style.Animation);
                        }
                    });
                    popup.dismiss();
                    recyclerView.scrollToPosition(check2);

                }
            }.start();


        }else{
            LayoutInflater li = getLayoutInflater();
            View layout = li.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.customtos));
            Toast toast = new Toast(getApplicationContext());
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setView(layout);//setting the view of custom toast layout
            toast.show();
        }
//        aAdapter.setListData(listData);
//        aAdapter.notifyDataSetChanged();

    }

    @Override
    public void option3Onclick(View v, int p) {
        final PopupWindow popup;
        Activity2model item = (Activity2model) listData.get(p);
        String option3 = item.getOption3();
        String answer = item.getAnswer();
        String answercheck = item.getAnswercheck();
        final int check2 = item.getCheck2();
        String question1 = item.getQuestion1();
        final String question2 = item.getQuestion2();

        String question1visibility = item.getQuestion1visibility();
        String question2visibility = item.getQuestion2visibility();
        String answervisibility = item.getAnswervisibility();


        Random r = new Random();
        int adv = r.nextInt(10 - 1) + 1;

        if(adv == 8 || adv == 5 || adv == 3){
            InterstitialAd mInterstitialAd;
            final InterstitialAd interstitial;

            AdRequest adRequest = new AdRequest.Builder().build();

            // Prepare the Interstitial Ad
            interstitial = new InterstitialAd(Activity2.this);
// Insert the Ad Unit ID
            interstitial.setAdUnitId(getString(R.string.admob_interstitial_id));

            interstitial.loadAd(adRequest);
// Prepare an Interstitial Ad Listener
            interstitial.setAdListener(new AdListener() {
                public void onAdLoaded() {
// Call displayInterstitial() function
                    // If Ads are loaded, show Interstitial else show nothing.
                    if (interstitial.isLoaded()) {
                        interstitial.show();
                    }
                }
            });
        }

        if (option3 == answercheck){
            LayoutInflater li = (LayoutInflater)getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
            View layout = li.inflate(R.layout.custom_toast2, (ViewGroup) findViewById(R.id.custom_toast2));



            View tview1 =  layout.findViewById(R.id.tview1);
            View tview2 =  layout.findViewById(R.id.tview2);
            View tview3 =  layout.findViewById(R.id.tview3);
            tview1.setVisibility(View.INVISIBLE);
            tview2.setVisibility(View.INVISIBLE);
            tview3.setVisibility(View.INVISIBLE);

            ImageView ivrighticon1 = (ImageView) layout.findViewById(R.id.righticon1);
            ImageView ivrighticon2 = (ImageView) layout.findViewById(R.id.righticon2);
            ImageView ivrighticon3 = (ImageView) layout.findViewById(R.id.righticon3);

            ivrighticon1.setVisibility(View.INVISIBLE);
            ivrighticon2.setVisibility(View.INVISIBLE);
            ivrighticon3.setVisibility(View.INVISIBLE);

            if (question1visibility == "v" && question2visibility == "v" && answervisibility == "i"){
                tview2.setVisibility(View.VISIBLE);
                ivrighticon2.setVisibility(View.VISIBLE);
            }
            else if (question1visibility == "v" && question2visibility == "i" && answervisibility == "v"){
                tview3.setVisibility(View.VISIBLE);
                ivrighticon3.setVisibility(View.VISIBLE);
            }
            else if (question1visibility == "i" && question2visibility == "v" && answervisibility == "v"){
                tview1.setVisibility(View.VISIBLE);
                ivrighticon1.setVisibility(View.VISIBLE);
            }


            TextView t_question1 = (TextView) layout.findViewById(R.id.toast_question1);
            final Typeface font = Typeface.createFromAsset(getAssets(), "fonts/custom.TTF");
            t_question1.setTypeface(font);
            t_question1.setText(question1);

            TextView t_question2 = (TextView) layout.findViewById(R.id.toast_question2);
            t_question2.setTypeface(font);

            t_question2.setText(question2);

            TextView t_answer = (TextView) layout.findViewById(R.id.toast_answer);
            t_answer.setTypeface(font);
            t_answer.setText(answer);

            popup = new PopupWindow(layout, RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.MATCH_PARENT,true);
            popup.setAnimationStyle(R.style.Animation);
            popup.showAtLocation(layout, Gravity.CENTER, 0, 0);


            new CountDownTimer(2000, 1000) {

                @Override
                public void onTick(long millisUntilFinished) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onFinish() {
                    // TODO Auto-generated method stub

                    popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                        @Override
                        public void onDismiss() {
                            popup.setAnimationStyle(R.style.Animation);
                        }
                    });
                    popup.dismiss();
                    recyclerView.scrollToPosition(check2);
                    if(question2 == "Y"){
                        final PopupWindow popup1;
                        LayoutInflater li1 = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                        View layout1 = li1.inflate(R.layout.level_clear_greeting, (ViewGroup) findViewById(R.id.level));

                        TextView level_text = (TextView) layout1.findViewById(R.id.textview_level_cleared);
                        level_text.setTypeface(font);

                        TextView congrats = (TextView) layout1.findViewById(R.id.textview_congrats);
                        congrats.setTypeface(font);

                        popup1 = new PopupWindow(layout1, RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.MATCH_PARENT, true);

                        popup1.showAtLocation(layout1, Gravity.CENTER, 0, 0);

                        InterstitialAd mInterstitialAd;
                        final InterstitialAd interstitial;

                        AdRequest adRequest = new AdRequest.Builder().build();

                        // Prepare the Interstitial Ad
                        interstitial = new InterstitialAd(Activity2.this);
// Insert the Ad Unit ID
                        interstitial.setAdUnitId(getString(R.string.admob_interstitial_id));

                        interstitial.loadAd(adRequest);
// Prepare an Interstitial Ad Listener
                        interstitial.setAdListener(new AdListener() {
                            public void onAdLoaded() {
// Call displayInterstitial() function
                                // If Ads are loaded, show Interstitial else show nothing.
                                if (interstitial.isLoaded()) {
                                    interstitial.show();
                                }
                            }
                        });


                        new CountDownTimer(6000, 1000) {

                            @Override
                            public void onTick(long millisUntilFinished) {
                                // TODO Auto-generated method stub

                            }

                            @Override
                            public void onFinish() {
                                // TODO Auto-generated method stub
                                popup1.dismiss();
                                finish();
                                Intent intent = new Intent(Activity2.this, Activity3.class);
                                 startActivity(intent);
                            }
                        }.start();

                    }
                }
            }.start();



        }else{
            LayoutInflater li = getLayoutInflater();
            View layout = li.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.customtos));
            Toast toast = new Toast(getApplicationContext());
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setView(layout);//setting the view of custom toast layout
            toast.show();
        }
//        aAdapter.setListData(listData);
//        aAdapter.notifyDataSetChanged();

    }

    @Override
    public void option4Onclick(View v, int p) {
        final PopupWindow popup;
        Activity2model item = (Activity2model) listData.get(p);
        String option4 = item.getOption4();
        String answer = item.getAnswer();
        String answercheck = item.getAnswercheck();
        final int check2 = item.getCheck2();
        String question1 = item.getQuestion1();
        final String question2 = item.getQuestion2();
        String question1visibility = item.getQuestion1visibility();
        String question2visibility = item.getQuestion2visibility();
        String answervisibility = item.getAnswervisibility();


        Random r = new Random();
        int adv = r.nextInt(10 - 1) + 1;

        if(adv == 8 || adv == 5 || adv == 3){
            InterstitialAd mInterstitialAd;
            final InterstitialAd interstitial;

            AdRequest adRequest = new AdRequest.Builder().build();

            // Prepare the Interstitial Ad
            interstitial = new InterstitialAd(Activity2.this);
// Insert the Ad Unit ID
            interstitial.setAdUnitId(getString(R.string.admob_interstitial_id));

            interstitial.loadAd(adRequest);
// Prepare an Interstitial Ad Listener
            interstitial.setAdListener(new AdListener() {
                public void onAdLoaded() {
// Call displayInterstitial() function
                    // If Ads are loaded, show Interstitial else show nothing.
                    if (interstitial.isLoaded()) {
                        interstitial.show();
                    }
                }
            });
        }

        if (option4 == answercheck){
            LayoutInflater li = (LayoutInflater)getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
            View layout = li.inflate(R.layout.custom_toast2, (ViewGroup) findViewById(R.id.custom_toast2));


            View tview1 =  layout.findViewById(R.id.tview1);
            View tview2 =  layout.findViewById(R.id.tview2);
            View tview3 =  layout.findViewById(R.id.tview3);
            tview1.setVisibility(View.INVISIBLE);
            tview2.setVisibility(View.INVISIBLE);
            tview3.setVisibility(View.INVISIBLE);

            ImageView ivrighticon1 = (ImageView) layout.findViewById(R.id.righticon1);
            ImageView ivrighticon2 = (ImageView) layout.findViewById(R.id.righticon2);
            ImageView ivrighticon3 = (ImageView) layout.findViewById(R.id.righticon3);
            ivrighticon1.setVisibility(View.INVISIBLE);
            ivrighticon2.setVisibility(View.INVISIBLE);
            ivrighticon3.setVisibility(View.INVISIBLE);

            if (question1visibility == "v" && question2visibility == "v" && answervisibility == "i"){
                  tview2.setVisibility(View.VISIBLE);
                 ivrighticon2.setVisibility(View.VISIBLE);
            }
            else if (question1visibility == "v" && question2visibility == "i" && answervisibility == "v"){
                tview3.setVisibility(View.VISIBLE);
                ivrighticon3.setVisibility(View.VISIBLE);
            }
            else if (question1visibility == "i" && question2visibility == "v" && answervisibility == "v"){
                tview1.setVisibility(View.VISIBLE);
                ivrighticon1.setVisibility(View.VISIBLE);
            }


            TextView t_question1 = (TextView) layout.findViewById(R.id.toast_question1);
            Typeface font = Typeface.createFromAsset(getAssets(), "fonts/custom.TTF");
            t_question1.setTypeface(font);
            t_question1.setText(question1);

            TextView t_question2 = (TextView) layout.findViewById(R.id.toast_question2);
            t_question2.setTypeface(font);
            t_question2.setText(question2);

            TextView t_answer = (TextView) layout.findViewById(R.id.toast_answer);
            t_answer.setTypeface(font);
            t_answer.setText(answer);

            popup = new PopupWindow(layout, RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.MATCH_PARENT,true);
            popup.setAnimationStyle(R.style.Animation);
            popup.showAtLocation(layout, Gravity.CENTER, 0, 0);


            new CountDownTimer(2000, 1000) {

                @Override
                public void onTick(long millisUntilFinished) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onFinish() {
                    // TODO Auto-generated method stub

                    popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                        @Override
                        public void onDismiss() {
                            popup.setAnimationStyle(R.style.Animation);
                        }
                    });
                    popup.dismiss();
                    recyclerView.scrollToPosition(check2);

                }
            }.start();


        }else{
            LayoutInflater li = getLayoutInflater();
            View layout = li.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.customtos));
            Toast toast = new Toast(getApplicationContext());
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setView(layout);//setting the view of custom toast layout
            toast.show();
        }
//        aAdapter.setListData(listData);
//        aAdapter.notifyDataSetChanged();

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void fab2Onclick(View v, int p) {
        Activity2model item = (Activity2model) listData.get(p);
        int check2 = item.getCheck2();

        Random r = new Random();
        int adv = r.nextInt(10 - 1) + 1;

        if(adv == 8 || adv == 5 || adv == 3){
            InterstitialAd mInterstitialAd;
            final InterstitialAd interstitial;

            AdRequest adRequest = new AdRequest.Builder().build();

            // Prepare the Interstitial Ad
            interstitial = new InterstitialAd(Activity2.this);
// Insert the Ad Unit ID
            interstitial.setAdUnitId(getString(R.string.admob_interstitial_id));

            interstitial.loadAd(adRequest);
// Prepare an Interstitial Ad Listener
            interstitial.setAdListener(new AdListener() {
                public void onAdLoaded() {
// Call displayInterstitial() function
                    // If Ads are loaded, show Interstitial else show nothing.
                    if (interstitial.isLoaded()) {
                        interstitial.show();
                    }
                }
            });
        }
        if(check2== 1){
            finishAffinity();
            Intent intent = new Intent(Activity2.this, Home.class);
            startActivity(intent);
        } else{
            recyclerView.scrollToPosition(check2 - 2);
        }
        aAdapter.setListData(listData);
        aAdapter.notifyDataSetChanged();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void homebuttonclick(int p) {

        Random r = new Random();
        int adv = r.nextInt(10 - 1) + 1;

        if(adv == 8 || adv == 5 || adv == 3){
            InterstitialAd mInterstitialAd;
            final InterstitialAd interstitial;

            AdRequest adRequest = new AdRequest.Builder().build();

            // Prepare the Interstitial Ad
            interstitial = new InterstitialAd(Activity2.this);
// Insert the Ad Unit ID
            interstitial.setAdUnitId(getString(R.string.admob_interstitial_id));

            interstitial.loadAd(adRequest);
// Prepare an Interstitial Ad Listener
            interstitial.setAdListener(new AdListener() {
                public void onAdLoaded() {
// Call displayInterstitial() function
                    // If Ads are loaded, show Interstitial else show nothing.
                    if (interstitial.isLoaded()) {
                        interstitial.show();
                    }
                }
            });
        }
        finishAffinity();
        Intent intent = new Intent (Activity2.this,Home.class);
        startActivity(intent);
    }

    @Override
    public void backlinkbutton(int p) {
        Activity2model item = (Activity2model) listData.get(p);
        int check2 = item.getCheck2();
        recyclerView.scrollToPosition(check2-2);
        aAdapter.setListData(listData);
        aAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main2, menu);
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_home) {
            finishAffinity();
            Intent intent = new Intent (Activity2.this,Home.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Log.i(TAG, "Setting screen name: " + name);
        mTracker.setScreenName("Image~" + name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Share")
                .build());

    }
}
