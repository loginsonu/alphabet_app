package com.techstern.alphabet_app;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sonu on 11/12/16.
 */

public class Seeddata2 {
    private static final String[] question1 = new String[]{
            "A",
            "C",
            "E",
            "G",
            "I",
            "K",
            "M",
            "O",
            "Q",
            "S",
            "U",
            "W" //12
    };
    private static final String[] question1visibility = new String[]{
            //v = visible
            //i = invisible
            "v",
            "v",
            "v",
            "v",
            "i",
            "v",
            "v",
            "v",
            "i",
            "v",
            "v",
            "v"//12

    };



    private static final String[] question2 = new String[]{
            "C",
            "E",
            "G",
            "I",
            "k",
            "M",
            "O",
            "Q",
            "S",
            "U",
            "W",
            "Y"//12
    };

    private static final String[] answervisibility = new String[]{
            //v = visible
            //i = invisible
            "i",
            "v",
            "v",
            "i", //4
            "v",
            "v",
            "i",
            "i",//8
            "v",
            "v",
            "i",
            "v"//12

    };

    private static final String[] answer = new String[]{
            "B",
            "D",
            "F",
            "H",
            "J",
            "L",
            "N",
            "P",
            "R",
            "T",
            "V",
            "X"//12

    };

    private static final String[]  question2visibility = new String[]{
            //v = visible
            //i = invisible
            "v",
            "i",
            "i",
            "v", //4
            "v",
            "i",
            "v",
            "v",//8
            "v",
            "i",
            "v",
            "i"//12

    };

    private static final String[] option1 = new String[]{
            "F",
            "A",
            "G",
            "H",
            "D",
            "P",
            "N",
            "A",
            "Q",
            "F",
            "C",
            "F" //12
    };
    private static final String[] option2 = new String[]{
            "G",
            "R",
            "I",
            "M",
            "I",
            "Q",
            "Z",
            "E",
            "E",
            "X",
            "R",
            "L" //12
    };

    private static final String[] option3 = new String[]{
            "B",
            "T",
            "Z",
            "L",
            "R",
            "M",
            "T",
            "L",
            "G",
            "U",
            "G",
            "Y"//12
    };

    private static final String[] option4 = new String[]{
            "K",
            "E",
            "X",
            "O",
            "S",
            "T",
            "V",
            "P",
            "O",
            "Z",
            "V",
            "M" //12
    };

    private static final String[] answercheck = new String[]{
            "B",
            "E",
            "G",
            "H",
            "I",
            "M",
            "N",
            "P",
            "Q",
            "U",
            "V",
            "Y"//12
    };

    private static final int[] check2 = new int[] {1,2,3,4,5,6,7,8,9,10,11,12};

    public static List<Activity2model> getListData(){
        List<Activity2model> data = new ArrayList<>();
        for (int i = 0; i < question1.length; i++)
        {

            Activity2model ac2m = new Activity2model(question1[i], question2[i],
                    answer[i], option1[i], option2[i], option3[i], option4[i], check2[i],
                    question1visibility[i],question2visibility[i],answervisibility[i], answercheck[i]);
            // Binds all strings into an array
            data.add(ac2m);

        }
        return data;
    }

}
