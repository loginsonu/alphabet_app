package com.techstern.alphabet_app;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by sonu on 11/9/16.
 */

public class Activity1adapter extends RecyclerView.Adapter<Activity1adapter.MyViewHolder>{

    private List<Activity1model> alphabetList;
    private ItemClickCallBack itemClickCallBack;

    public interface ItemClickCallBack {
        void iconImageView1OnClick(View v, int p);
        void iconImageView2OnClick(View v, int p);
        void iconImageView3OnClick( View v, int p);
        void iconImageViewfabOnClick( int p);
        void homebuttonclick(int p);
        void backlinkbutton(int p);


    }

    public void setItemClickCallBack(final ItemClickCallBack itemClickCallBack){
        this.itemClickCallBack = itemClickCallBack;
    }



    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView title;
        public ImageView img1,img2,img3,floting,backlink,homebutton;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.alpha);
            Typeface font = Typeface.createFromAsset(itemView.getContext().getAssets(), "fonts/custom.TTF");
            title.setTypeface(font);

            img1 = (ImageView) view.findViewById(R.id.imageview1);
            img2 = (ImageView) view.findViewById(R.id.imageview2);
            img3 = (ImageView) view.findViewById(R.id.imageview3);
            floting = (ImageView) view.findViewById(R.id.fab);
            backlink = (ImageView) view.findViewById(R.id.backclick);
            homebutton = (ImageView) view.findViewById(R.id.homebutton);


            img1.setOnClickListener(this);
            img2.setOnClickListener(this);
            img3.setOnClickListener(this);
            floting.setOnClickListener(this);
            backlink.setOnClickListener(this);
            homebutton.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
             if(v.getId() == R.id.imageview1){
              itemClickCallBack.iconImageView1OnClick(v, getAdapterPosition());
             }
            else if(v.getId() == R.id.imageview2) {
                 itemClickCallBack.iconImageView2OnClick(v, getAdapterPosition());
             }
             else if(v.getId() == R.id.imageview3) {
                 itemClickCallBack.iconImageView3OnClick(v, getAdapterPosition());
             }
            else if (v.getId() == R.id.fab){
                 itemClickCallBack.iconImageViewfabOnClick(getAdapterPosition());
             }
             else if (v.getId() == R.id.backclick){
                 itemClickCallBack.backlinkbutton(getAdapterPosition());
             }
             else if (v.getId() == R.id.homebutton){
                 itemClickCallBack.homebuttonclick(getAdapterPosition());
             }
        }
    }


    public Activity1adapter(List<Activity1model> alphabetList) {
        this.alphabetList = alphabetList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.itemstyleactivity1, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Activity1model alphabet = alphabetList.get(position);

        holder.title.setText(alphabet.getAlphabet());
        holder.img1.setImageResource(alphabet.getImage1());

        holder.img2.setImageResource(alphabet.getImage2());

        holder.img3.setImageResource(alphabet.getImage3());



    }

    public void setListData(List<Activity1model> exerciseList) {
        this.alphabetList.clear();
        this.alphabetList.addAll(exerciseList);
    }

    @Override
    public int getItemCount() {
        return alphabetList.size();
    }
}
