package com.techstern.alphabet_app;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;
import java.util.Random;

import static android.R.attr.name;

public class MainActivity extends AppCompatActivity implements Activity1adapter.ItemClickCallBack {

    private ArrayList listData;

    private RecyclerView recyclerView; //recyclerobject

    private Activity1adapter aAdapter; // adapterobject

    LinearLayoutManager layoutManager;
    private Tracker mTracker;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();


        listData = (ArrayList) Seeddata.getListData();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        aAdapter = new Activity1adapter(Seeddata.getListData());



        layoutManager = new LinearLayoutManager(this) {
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }
            public boolean canScrollVertically() {
                return false;
            }
        };
        recyclerView.setLayoutManager(layoutManager);


        recyclerView.setAdapter(aAdapter);
        aAdapter.setItemClickCallBack(this);


    }


    @Override
    public void iconImageView1OnClick(View v, int p) {
        final PopupWindow popup;
        Activity1model item = (Activity1model) listData.get(p);
        String alpha = item.getAlphabet();
        String tagg = item.getTag();
        final int check = item.getCheck();
        final String spelling = item.getSpelling();
        ImageView firstimage = (ImageView) v.findViewById(R.id.imageview1);


        Random r = new Random();
        int adv = r.nextInt(10 - 1) + 1;

        if(adv == 8 || adv == 5 || adv == 3){
            InterstitialAd mInterstitialAd;
            final InterstitialAd interstitial;

            AdRequest adRequest = new AdRequest.Builder().build();

            // Prepare the Interstitial Ad
            interstitial = new InterstitialAd(MainActivity.this);
// Insert the Ad Unit ID
            interstitial.setAdUnitId(getString(R.string.admob_interstitial_id));

            interstitial.loadAd(adRequest);
// Prepare an Interstitial Ad Listener
            interstitial.setAdListener(new AdListener() {
                public void onAdLoaded() {
// Call displayInterstitial() function
                    // If Ads are loaded, show Interstitial else show nothing.
                    if (interstitial.isLoaded()) {
                        interstitial.show();
                    }
                }
            });
        }


        if (alpha == tagg) {


            LayoutInflater li = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);

            View layout = li.inflate(R.layout.custom_toast1, (ViewGroup) findViewById(R.id.customtos1));
            final Typeface font = Typeface.createFromAsset(getAssets(), "fonts/custom.TTF");

            TextView t_spelling = (TextView) layout.findViewById(R.id.spelling);
            t_spelling.setTypeface(font);
            t_spelling.setText(spelling);

            ImageView correctimage = (ImageView) layout.findViewById(R.id.correctImage);

            correctimage.setImageDrawable(firstimage.getDrawable());
            popup = new PopupWindow(layout, RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.MATCH_PARENT, true);
            popup.setAnimationStyle(R.style.Animation);
            popup.showAtLocation(layout, Gravity.CENTER, 0, 0);


            new CountDownTimer(2000, 1000) {

                @Override
                public void onTick(long millisUntilFinished) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onFinish() {
                    // TODO Auto-generated method stub

                    popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                        @Override
                        public void onDismiss() {
                            popup.setAnimationStyle(R.style.Animation);
                        }
                    });
                    popup.dismiss();
                    recyclerView.scrollToPosition(check);









                    if (spelling == "Zebra") {


                        final PopupWindow popup1;
                        LayoutInflater li1 = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                        View layout1 = li1.inflate(R.layout.level_clear_greeting, (ViewGroup) findViewById(R.id.level));

                        InterstitialAd mInterstitialAd;
                        final InterstitialAd interstitial;

                        AdRequest adRequest = new AdRequest.Builder().build();

                        // Prepare the Interstitial Ad
                        interstitial = new InterstitialAd(MainActivity.this);
// Insert the Ad Unit ID
                        interstitial.setAdUnitId(getString(R.string.admob_interstitial_id));

                        interstitial.loadAd(adRequest);
// Prepare an Interstitial Ad Listener
                        interstitial.setAdListener(new AdListener() {
                            public void onAdLoaded() {
// Call displayInterstitial() function
                                // If Ads are loaded, show Interstitial else show nothing.
                                if (interstitial.isLoaded()) {
                                    interstitial.show();
                                }
                            }
                        });





                        TextView level_text = (TextView) layout1.findViewById(R.id.textview_level_cleared);
                        level_text.setTypeface(font);

                        TextView congrats = (TextView) layout1.findViewById(R.id.textview_congrats);
                        congrats.setTypeface(font);

                        popup1 = new PopupWindow(layout1, RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.MATCH_PARENT, true);

                        popup1.showAtLocation(layout1, Gravity.CENTER, 0, 0);


                        new CountDownTimer(6000, 1000) {

                            @Override
                            public void onTick(long millisUntilFinished) {
                                // TODO Auto-generated method stub

                            }

                            @Override
                            public void onFinish() {
                                // TODO Auto-generated method stub
                                popup1.dismiss();
                                finish();
                                Intent intent = new Intent(MainActivity.this, Activity2.class);
                                startActivity(intent);
                            }
                        }.start();




                    }






                }
            }.start();











        } else {
            LayoutInflater li = getLayoutInflater();
            View layout = li.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.customtos));

            Toast toast = new Toast(getApplicationContext());
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setView(layout); //setting the view of custom toast layout
            toast.show();

        }
        aAdapter.setListData(listData);
        aAdapter.notifyDataSetChanged();
    }

    @Override
    public void iconImageView2OnClick(View v, int p) {
        final PopupWindow popup;
        Activity1model item = (Activity1model) listData.get(p);
        String alpha = item.getAlphabet();
        String tagg1 = item.getTag1();
        final int check = item.getCheck();
        final String spelling = item.getSpelling();
        ImageView secondimage = (ImageView) v.findViewById(R.id.imageview2);


        Random r = new Random();
        int adv = r.nextInt(10 - 1) + 1;

        if(adv == 8 || adv == 5 || adv == 3){
            InterstitialAd mInterstitialAd;
            final InterstitialAd interstitial;

            AdRequest adRequest = new AdRequest.Builder().build();

            // Prepare the Interstitial Ad
            interstitial = new InterstitialAd(MainActivity.this);
// Insert the Ad Unit ID
            interstitial.setAdUnitId(getString(R.string.admob_interstitial_id));

            interstitial.loadAd(adRequest);
// Prepare an Interstitial Ad Listener
            interstitial.setAdListener(new AdListener() {
                public void onAdLoaded() {
// Call displayInterstitial() function
                    // If Ads are loaded, show Interstitial else show nothing.
                    if (interstitial.isLoaded()) {
                        interstitial.show();
                    }
                }
            });
        }



        if (alpha == tagg1) {
            LayoutInflater li = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
            View layout = li.inflate(R.layout.custom_toast1, (ViewGroup) findViewById(R.id.customtos1));
            Typeface font = Typeface.createFromAsset(getAssets(), "fonts/custom.TTF");
            TextView t_spelling = (TextView) layout.findViewById(R.id.spelling);
            t_spelling.setTypeface(font);

            t_spelling.setText(spelling);

            ImageView correctimage = (ImageView) layout.findViewById(R.id.correctImage);

            correctimage.setImageDrawable(secondimage.getDrawable());

            popup = new PopupWindow(layout, RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.MATCH_PARENT, true);
            popup.setAnimationStyle(R.style.Animation);
            popup.showAtLocation(layout, Gravity.CENTER, 0, 0);


            new CountDownTimer(2000, 1000) {

                @Override
                public void onTick(long millisUntilFinished) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onFinish() {
                    // TODO Auto-generated method stub

                    popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                        @Override
                        public void onDismiss() {
                            popup.setAnimationStyle(R.style.Animation);
                        }
                    });
                    popup.dismiss();
                    recyclerView.scrollToPosition(check);
                    if (spelling == "Zebra") {
                        Intent intent = new Intent(MainActivity.this, Activity2.class);
                        startActivity(intent);
                    }
                }
            }.start();




        } else {
            LayoutInflater li = getLayoutInflater();
            View layout = li.inflate(R.layout.custom_toast,
                    (ViewGroup) findViewById(R.id.customtos));
            Toast toast = new Toast(getApplicationContext());
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setView(layout); //setting the view of custom toast layout
            toast.show();
        }
        aAdapter.setListData(listData);
        aAdapter.notifyDataSetChanged();

    }

    @Override
    public void iconImageView3OnClick(View v, int p) {
        final PopupWindow popup;

        Activity1model item = (Activity1model) listData.get(p);
        String alpha = item.getAlphabet();
        String tagg2 = item.getTag2();
        final int check = item.getCheck();
        final String spelling = item.getSpelling();
        ImageView thirdimage = (ImageView) v.findViewById(R.id.imageview3);



        Random r = new Random();
        int adv = r.nextInt(10 - 1) + 1;

        if(adv == 8 || adv == 5 || adv == 3){
            InterstitialAd mInterstitialAd;
            final InterstitialAd interstitial;

            AdRequest adRequest = new AdRequest.Builder().build();

            // Prepare the Interstitial Ad
            interstitial = new InterstitialAd(MainActivity.this);
// Insert the Ad Unit ID
            interstitial.setAdUnitId(getString(R.string.admob_interstitial_id));

            interstitial.loadAd(adRequest);
// Prepare an Interstitial Ad Listener
            interstitial.setAdListener(new AdListener() {
                public void onAdLoaded() {
// Call displayInterstitial() function
                    // If Ads are loaded, show Interstitial else show nothing.
                    if (interstitial.isLoaded()) {
                        interstitial.show();
                    }
                }
            });
        }





        if (alpha == tagg2) {
            LayoutInflater li = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
            View layout = li.inflate(R.layout.custom_toast1, (ViewGroup) findViewById(R.id.customtos1));



            TextView t_spelling = (TextView) layout.findViewById(R.id.spelling);
            Typeface font = Typeface.createFromAsset(getAssets(), "fonts/custom.TTF");
            t_spelling.setTypeface(font);
            t_spelling.setText(spelling);

            ImageView correctimage = (ImageView) layout.findViewById(R.id.correctImage);

            correctimage.setImageDrawable(thirdimage.getDrawable());

            popup = new PopupWindow(layout, RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.MATCH_PARENT, true);
            popup.setAnimationStyle(R.style.Animation);
            popup.showAtLocation(layout, Gravity.CENTER, 0, 0);



            new CountDownTimer(2000, 1000) {

                @Override
                public void onTick(long millisUntilFinished) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onFinish() {
                    // TODO Auto-generated method stub

                    popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                        @Override
                        public void onDismiss() {
                            popup.setAnimationStyle(R.style.Animation);
                        }
                    });
                    popup.dismiss();
                    recyclerView.scrollToPosition(check);
                    if (spelling == "Zebra") {

                        Intent intent = new Intent(MainActivity.this, Activity2.class);
                        startActivity(intent);
                    }
                }
            }.start();


            //            recyclerView.scrollToPosition(check);

        } else {
            LayoutInflater li = getLayoutInflater();
            View layout = li.inflate(R.layout.custom_toast,
                    (ViewGroup) findViewById(R.id.customtos));

            Toast toast = new Toast(getApplicationContext());
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setView(layout); //setting the view of custom toast layout
            toast.show();
        }
        aAdapter.setListData(listData);
        aAdapter.notifyDataSetChanged();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void iconImageViewfabOnClick(int p) {
        Activity1model item = (Activity1model) listData.get(p);
        int check = item.getCheck();
//        recyclerView.scrollToPosition(check - 2);

        Random r = new Random();
        int adv = r.nextInt(10 - 1) + 1;

        if(adv == 8 || adv == 5 || adv == 3){
            InterstitialAd mInterstitialAd;
            final InterstitialAd interstitial;

            AdRequest adRequest = new AdRequest.Builder().build();

            // Prepare the Interstitial Ad
            interstitial = new InterstitialAd(MainActivity.this);
// Insert the Ad Unit ID
            interstitial.setAdUnitId(getString(R.string.admob_interstitial_id));

            interstitial.loadAd(adRequest);
// Prepare an Interstitial Ad Listener
            interstitial.setAdListener(new AdListener() {
                public void onAdLoaded() {
// Call displayInterstitial() function
                    // If Ads are loaded, show Interstitial else show nothing.
                    if (interstitial.isLoaded()) {
                        interstitial.show();
                    }
                }
            });
        }

        if(check== 1){
            finishAffinity();
            Intent intent = new Intent(MainActivity.this, Home.class);
            startActivity(intent);
        } else{
            recyclerView.scrollToPosition(check - 2);
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void homebuttonclick(int p) {
        Random r = new Random();
        int adv = r.nextInt(10 - 1) + 1;

        if(adv == 8 || adv == 5 || adv == 3){
            InterstitialAd mInterstitialAd;
            final InterstitialAd interstitial;

            AdRequest adRequest = new AdRequest.Builder().build();

            // Prepare the Interstitial Ad
            interstitial = new InterstitialAd(MainActivity.this);
// Insert the Ad Unit ID
            interstitial.setAdUnitId(getString(R.string.admob_interstitial_id));

            interstitial.loadAd(adRequest);
// Prepare an Interstitial Ad Listener
            interstitial.setAdListener(new AdListener() {
                public void onAdLoaded() {
// Call displayInterstitial() function
                    // If Ads are loaded, show Interstitial else show nothing.
                    if (interstitial.isLoaded()) {
                        interstitial.show();
                    }
                }
            });
        }
        finishAffinity();
        Intent intent = new Intent(MainActivity.this, Home.class);
        startActivity(intent);
    }

    @Override
    public void backlinkbutton(int p) {
        Activity1model item = (Activity1model) listData.get(p);
        int check = item.getCheck();
        recyclerView.scrollToPosition(check - 2);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main2, menu);
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_home) {
            finishAffinity();
            Intent intent = new Intent(MainActivity.this, Home.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Log.i(TAG, "Setting screen name: " + name);
        mTracker.setScreenName("Image~" + name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Share")
                .build());

    }
}