package com.techstern.alphabet_app;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by sonu on 11/9/16.
 */

public class Activity2adapter extends RecyclerView.Adapter<Activity2adapter.MyViewHolder>{

    private List<Activity2model> activity2modelList;
    private ItemClickCallBack itemClickCallBack;

    public interface ItemClickCallBack {
        void option1Onclick(View v, int p);
        void option2Onclick(View v, int p);
        void option3Onclick(View v, int p);
        void option4Onclick(View v, int p);
        void fab2Onclick(View v, int p);
        void homebuttonclick(int p);
        void backlinkbutton(int p);
    }

    public void setItemClickCallBack(final ItemClickCallBack itemClickCallBack){
        this.itemClickCallBack = itemClickCallBack;
    }



    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView question1, question2, option1, option2, option3, option4, answerr;
        public ImageView fab2,backlink2,homebutton2;
        public View view1,view2, view3;

//        public ImageView img1,img2,img3,floting;

        public MyViewHolder(View view) {
            super(view);
            question1 = (TextView) view.findViewById(R.id.question1);
            Typeface font = Typeface.createFromAsset(itemView.getContext().getAssets(), "fonts/custom.TTF");
            question1.setTypeface(font);

            question2 = (TextView) view.findViewById(R.id.question2);
            question2.setTypeface(font);

            answerr = (TextView) view.findViewById(R.id.answer);
            answerr.setTypeface(font);

            option1 = (TextView) view.findViewById(R.id.option1);
            option1.setTypeface(font);

            option2 = (TextView) view.findViewById(R.id.option2);
            option2.setTypeface(font);

            option3 = (TextView) view.findViewById(R.id.option3);
            option3.setTypeface(font);

            option4 = (TextView) view.findViewById(R.id.option4);
            option4.setTypeface(font);

            fab2 = (ImageView) view.findViewById(R.id.fab2);
            backlink2 = (ImageView) view.findViewById(R.id.backclick2);
            homebutton2 = (ImageView) view.findViewById(R.id.homebutton2);

            view1 =  view.findViewById(R.id.view1);
            view2 =  view.findViewById(R.id.view2);
            view3 =  view.findViewById(R.id.view3);

//            title = (TextView) view.findViewById(R.id.alpha);
//            img1 = (ImageView) view.findViewById(R.id.imageview1);
//            img2 = (ImageView) view.findViewById(R.id.imageview2);
//            img3 = (ImageView) view.findViewById(R.id.imageview3);
//            floting = (ImageView) view.findViewById(R.id.fab);
//
//
//
             option1.setOnClickListener(this);
             option2.setOnClickListener(this);
             option3.setOnClickListener(this);
             option4.setOnClickListener(this);
             fab2.setOnClickListener(this);
            backlink2.setOnClickListener(this);
            homebutton2.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
             if(v.getId() == R.id.option1){
              itemClickCallBack.option1Onclick(v, getAdapterPosition());
                 Activity2model activity2model = activity2modelList.get(getAdapterPosition());
                 String question1visibility = activity2model.getQuestion1visibility();
                 String question2visibility = activity2model.getQuestion2visibility();
                 String answervisibility = activity2model.getAnswervisibility();

                 if(question1visibility == "v" && question2visibility == "v" && answervisibility == "i"){

                     answerr.setText(option1.getText());
                     answerr.setVisibility(View.VISIBLE);
                }
                 else if (question1visibility == "v" && question2visibility == "i" && answervisibility == "v"){
                    question2.setText(option1.getText());
                    question2.setVisibility(View.VISIBLE);
                }
                 else if (question1visibility == "i" && question2visibility == "v" && answervisibility == "v"){
                    question1.setText(option1.getText());
                    question1.setVisibility(View.VISIBLE);
                }

             }
            else if(v.getId() == R.id.option2) {
                 itemClickCallBack.option2Onclick(v, getAdapterPosition());

                 Activity2model activity2model = activity2modelList.get(getAdapterPosition());
                 String question1visibility = activity2model.getQuestion1visibility();
                 String question2visibility = activity2model.getQuestion2visibility();
                 String answervisibility = activity2model.getAnswervisibility();

                 if(question1visibility == "v" && question2visibility == "v" && answervisibility == "i"){

                     answerr.setText(option2.getText());
                     answerr.setVisibility(View.VISIBLE);
                 }
                 else if (question1visibility == "v" && question2visibility == "i" && answervisibility == "v"){
                     question2.setText(option2.getText());
                     question2.setVisibility(View.VISIBLE);
                 }
                 else if (question1visibility == "i" && question2visibility == "v" && answervisibility == "v"){
                     question1.setText(option2.getText());
                     question1.setVisibility(View.VISIBLE);
                 }

             }
             else if(v.getId() == R.id.option3) {
                 itemClickCallBack.option3Onclick(v, getAdapterPosition());

                 Activity2model activity2model = activity2modelList.get(getAdapterPosition());
                 String question1visibility = activity2model.getQuestion1visibility();
                 String question2visibility = activity2model.getQuestion2visibility();
                 String answervisibility = activity2model.getAnswervisibility();

                 if(question1visibility == "v" && question2visibility == "v" && answervisibility == "i"){

                     answerr.setText(option3.getText());
                     answerr.setVisibility(View.VISIBLE);
                 }
                 else if (question1visibility == "v" && question2visibility == "i" && answervisibility == "v"){
                     question2.setText(option3.getText());
                     question2.setVisibility(View.VISIBLE);
                 }
                 else if (question1visibility == "i" && question2visibility == "v" && answervisibility == "v"){
                     question1.setText(option3.getText());
                     question1.setVisibility(View.VISIBLE);
                 }


             }
            else if (v.getId() == R.id.option4){
                 itemClickCallBack.option4Onclick(v, getAdapterPosition());

                 Activity2model activity2model = activity2modelList.get(getAdapterPosition());
                 String question1visibility = activity2model.getQuestion1visibility();
                 String question2visibility = activity2model.getQuestion2visibility();
                 String answervisibility = activity2model.getAnswervisibility();

                 if(question1visibility == "v" && question2visibility == "v" && answervisibility == "i"){

                     answerr.setText(option4.getText());
                     answerr.setVisibility(View.VISIBLE);
                 }
                 else if (question1visibility == "v" && question2visibility == "i" && answervisibility == "v"){
                     question2.setText(option4.getText());
                     question2.setVisibility(View.VISIBLE);
                 }
                 else if (question1visibility == "i" && question2visibility == "v" && answervisibility == "v"){
                     question1.setText(option4.getText());
                     question1.setVisibility(View.VISIBLE);
                 }


             }
            else if (v.getId() == R.id.fab2){
                 itemClickCallBack.fab2Onclick(v, getAdapterPosition());
             }
             else if (v.getId() == R.id.backclick2){
                 itemClickCallBack.backlinkbutton(getAdapterPosition());
             }
             else if (v.getId() == R.id.homebutton2){
                 itemClickCallBack.homebuttonclick(getAdapterPosition());
             }
        }
    }


    public Activity2adapter(List<Activity2model> activity2modelList) {
        this.activity2modelList = activity2modelList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.itemstyleactivity2, parent, false);

//        tf_regular = Typeface.createFromAsset(itemView.getContext().getAssets(), "fonts/BebasNeue Regular.ttf");
//        this.mainText.setTypeface(tf_regular);
//        Typeface font = Typeface.createFromAsset(itemView.getContext().getAssets(), "fonts/custom.TTF");
//        this.question1.setTypeface(font);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Activity2model activity2model = activity2modelList.get(position);

        holder.question1.setText(activity2model.getQuestion1());
        holder.question2.setText(activity2model.getQuestion2());
        holder.answerr.setText(activity2model.getAnswer());


        holder.question1.setVisibility(View.INVISIBLE);
        holder.question2.setVisibility(View.INVISIBLE);
        holder.answerr.setVisibility(View.INVISIBLE);
        holder.view1.setVisibility(View.INVISIBLE);
        holder.view2.setVisibility(View.INVISIBLE);
        holder.view3.setVisibility(View.INVISIBLE);

        holder.option1.setText(activity2model.getOption1());
        holder.option2.setText(activity2model.getOption2());
        holder.option3.setText(activity2model.getOption3());
        holder.option4.setText(activity2model.getOption4());

       if(activity2model.getQuestion1visibility() == "v" && activity2model.getQuestion2visibility()== "v" &&
               activity2model.getAnswervisibility() == "i"){
           holder.question1.setVisibility(View.VISIBLE);
           holder.question2.setVisibility(View.VISIBLE);
           holder.view2.setVisibility(View.VISIBLE);

       }

        else if(activity2model.getQuestion1visibility() == "v" && activity2model.getQuestion2visibility()== "i" &&
               activity2model.getAnswervisibility() == "v"){
           holder.question1.setVisibility(View.VISIBLE);
           holder.answerr.setVisibility(View.VISIBLE);
           holder.view3.setVisibility(View.VISIBLE);
       }

       else if(activity2model.getQuestion1visibility() == "i" && activity2model.getQuestion2visibility()== "v" &&
               activity2model.getAnswervisibility() == "v"){
           holder.question2.setVisibility(View.VISIBLE);
           holder.answerr.setVisibility(View.VISIBLE);
           holder.view1.setVisibility(View.VISIBLE);
       }





    }

    public void setListData(List<Activity2model> exerciseList) {
        this.activity2modelList.clear();
        this.activity2modelList.addAll(exerciseList);

    }

    @Override
    public int getItemCount() {
        return activity2modelList.size();
    }
}
