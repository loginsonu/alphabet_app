package com.techstern.alphabet_app;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;
import java.util.Random;

import static android.R.attr.name;

public class Activity3 extends AppCompatActivity implements Activity3adapter.ItemClickCallBack {

    private ArrayList listData;

    private RecyclerView recyclerView; //recyclerobject

    private Activity3adapter aAdapter; // adapterobject

    LinearLayoutManager layoutManager;
    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_3);

        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();

        listData = (ArrayList) Seeddata3.getListData();
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_activity_3);
        aAdapter = new Activity3adapter(Seeddata3.getListData());
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this) {
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }
            public boolean  canScrollVertically() {
                return false;
            }
        };
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(aAdapter);
        aAdapter.setItemClickCallBack(this);
    }

    @Override
    public void option1Onclick(View v, int p) {
        final PopupWindow popup;
        Activity3model item = (Activity3model) listData.get(p);
        String alphabet3 = item.getAlphabet3();
        String optiontag1 = item.getOptiontag1();
        final String spelling3 = item.getSpelling3();
        final int check3 = item.getCheck3();
        int image1 = item.getOptionimage1();
        int questionimage1 = item.getQuestionimage1();
        int questionimage2 = item.getQuestionimage2();
        int questionimage3 = item.getQuestionimage3();

        String box1 = item.getBox1();
        String box2 = item.getBox2();
        String box3 = item.getBox3();

        Random r = new Random();
        int adv = r.nextInt(10 - 1) + 1;

        if(adv == 8 || adv == 5 || adv == 3){
            InterstitialAd mInterstitialAd;
            final InterstitialAd interstitial;

            AdRequest adRequest = new AdRequest.Builder().build();

            // Prepare the Interstitial Ad
            interstitial = new InterstitialAd(Activity3.this);
// Insert the Ad Unit ID
            interstitial.setAdUnitId(getString(R.string.admob_interstitial_id));

            interstitial.loadAd(adRequest);
// Prepare an Interstitial Ad Listener
            interstitial.setAdListener(new AdListener() {
                public void onAdLoaded() {
// Call displayInterstitial() function
                    // If Ads are loaded, show Interstitial else show nothing.
                    if (interstitial.isLoaded()) {
                        interstitial.show();
                    }
                }
            });
        }

        if(alphabet3 == optiontag1){

            LayoutInflater li = (LayoutInflater)getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
            View layout = li.inflate(R.layout.custom_toast3, (ViewGroup) findViewById(R.id.custom_toast3));

            TextView text = (TextView) layout.findViewById(R.id.question_text) ;
            text.setText(alphabet3);

            ImageView iv = (ImageView) layout.findViewById(R.id.question_image3);

            ImageView iv1 = (ImageView) layout.findViewById(R.id.question_image1);


            ImageView iv2 = (ImageView) layout.findViewById(R.id.question_image2);



            if(box1 == "v" && box2== "v" &&
                    box3 == "i"){

                iv.setImageResource(image1);
                iv1.setImageResource(questionimage1);
                iv2.setImageResource(questionimage2);
            }

            else if(box1 == "v" && box2== "i" &&
                    box3 == "v"){


                iv1.setImageResource(questionimage1);
                iv2.setImageResource(image1);
                iv.setImageResource(questionimage3);

            }

            else if(box1 == "i" && box2== "v" &&
                    box3 == "v"){

                iv1.setImageResource(image1);
                iv2.setImageResource(questionimage2);
                iv.setImageResource(questionimage3);

            }




            popup = new PopupWindow(layout, RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.MATCH_PARENT,true);
            popup.setAnimationStyle(R.style.Animation);
            popup.showAtLocation(layout, Gravity.CENTER, 0, 0);


            new CountDownTimer(2000, 1000) {

                @Override
                public void onTick(long millisUntilFinished) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onFinish() {
                    // TODO Auto-generated method stub

                    popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                        @Override
                        public void onDismiss() {
                            popup.setAnimationStyle(R.style.Animation);
                        }
                    });
                    popup.dismiss();
                    recyclerView.scrollToPosition(check3);



                    if (spelling3 == "Zebra") {

                        final Typeface font = Typeface.createFromAsset(getAssets(), "fonts/custom.TTF");
                        final PopupWindow popup1;
                        LayoutInflater li1 = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                        View layout1 = li1.inflate(R.layout.level_clear_greeting, (ViewGroup) findViewById(R.id.level));

                        TextView level_text = (TextView) layout1.findViewById(R.id.textview_level_cleared);
                        level_text.setTypeface(font);

                        TextView congrats = (TextView) layout1.findViewById(R.id.textview_congrats);
                        congrats.setTypeface(font);

                        popup1 = new PopupWindow(layout1, RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.MATCH_PARENT, true);

                        popup1.showAtLocation(layout1, Gravity.CENTER, 0, 0);

                        InterstitialAd mInterstitialAd;
                        final InterstitialAd interstitial;

                        AdRequest adRequest = new AdRequest.Builder().build();

                        // Prepare the Interstitial Ad
                        interstitial = new InterstitialAd(Activity3.this);
// Insert the Ad Unit ID
                        interstitial.setAdUnitId(getString(R.string.admob_interstitial_id));

                        interstitial.loadAd(adRequest);
// Prepare an Interstitial Ad Listener
                        interstitial.setAdListener(new AdListener() {
                            public void onAdLoaded() {
// Call displayInterstitial() function
                                // If Ads are loaded, show Interstitial else show nothing.
                                if (interstitial.isLoaded()) {
                                    interstitial.show();
                                }
                            }
                        });


                        new CountDownTimer(4000, 1000) {

                            @Override
                            public void onTick(long millisUntilFinished) {
                                // TODO Auto-generated method stub

                            }

                            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                            @Override
                            public void onFinish() {
                                // TODO Auto-generated method stub
                                popup1.dismiss();
                                finishAffinity();
                                Intent intent = new Intent(Activity3.this, Home.class);
                                startActivity(intent);
                            }
                        }.start();




                    }



                }
            }.start();

        }
        else{
            LayoutInflater li = getLayoutInflater();
            View layout = li.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.customtos));
            Toast toast = new Toast(getApplicationContext());
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setView(layout);//setting the view of custom toast layout
            toast.show();
        }
//        aAdapter.setListData(listData);
//        aAdapter.notifyDataSetChanged();

    }

    @Override
    public void option2Onclick(View v, int p) {
        final PopupWindow popup;

        Activity3model item = (Activity3model) listData.get(p);
        String alphabet3 = item.getAlphabet3();
        String optiontag2 = item.getOptiontag2();
        final int check3 = item.getCheck3();
        int image2 = item.getOptionimage2();
        int questionimage1 = item.getQuestionimage1();
        int questionimage2 = item.getQuestionimage2();
        int questionimage3 = item.getQuestionimage3();

        String box1 = item.getBox1();
        String box2 = item.getBox2();
        String box3 = item.getBox3();

        Random r = new Random();
        int adv = r.nextInt(10 - 1) + 1;

        if(adv == 8 || adv == 5 || adv == 3){
            InterstitialAd mInterstitialAd;
            final InterstitialAd interstitial;

            AdRequest adRequest = new AdRequest.Builder().build();

            // Prepare the Interstitial Ad
            interstitial = new InterstitialAd(Activity3.this);
// Insert the Ad Unit ID
            interstitial.setAdUnitId(getString(R.string.admob_interstitial_id));

            interstitial.loadAd(adRequest);
// Prepare an Interstitial Ad Listener
            interstitial.setAdListener(new AdListener() {
                public void onAdLoaded() {
// Call displayInterstitial() function
                    // If Ads are loaded, show Interstitial else show nothing.
                    if (interstitial.isLoaded()) {
                        interstitial.show();
                    }
                }
            });
        }

        if(alphabet3 == optiontag2){
            LayoutInflater li = (LayoutInflater)getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
            View layout = li.inflate(R.layout.custom_toast3, (ViewGroup) findViewById(R.id.custom_toast3));

            TextView text = (TextView) layout.findViewById(R.id.question_text) ;
            text.setText(alphabet3);

            ImageView iv = (ImageView) layout.findViewById(R.id.question_image3);

            ImageView iv1 = (ImageView) layout.findViewById(R.id.question_image1);

            ImageView iv2 = (ImageView) layout.findViewById(R.id.question_image2);



            if(box1 == "v" && box2== "v" &&
                    box3 == "i"){

                iv.setImageResource(image2);
                iv1.setImageResource(questionimage1);
                iv2.setImageResource(questionimage2);
            }

            else if(box1 == "v" && box2== "i" &&
                    box3 == "v"){


                iv1.setImageResource(questionimage1);
                iv2.setImageResource(image2);
                iv.setImageResource(questionimage3);

            }

            else if(box1 == "i" && box2== "v" &&
                    box3 == "v"){

                iv1.setImageResource(image2);
                iv2.setImageResource(questionimage2);
                iv.setImageResource(questionimage3);

            }



            popup = new PopupWindow(layout, RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.MATCH_PARENT,true);
            popup.setAnimationStyle(R.style.Animation);
            popup.showAtLocation(layout, Gravity.CENTER, 0, 0);


            new CountDownTimer(2000, 1000) {

                @Override
                public void onTick(long millisUntilFinished) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onFinish() {
                    // TODO Auto-generated method stub

                    popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                        @Override
                        public void onDismiss() {
                            popup.setAnimationStyle(R.style.Animation);
                        }
                    });
                    popup.dismiss();
                    recyclerView.scrollToPosition(check3);
                }
            }.start();
        }
        else{
            LayoutInflater li = getLayoutInflater();
            View layout = li.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.customtos));
            Toast toast = new Toast(getApplicationContext());
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setView(layout);//setting the view of custom toast layout
            toast.show();
        }
//        aAdapter.setListData(listData);
//        aAdapter.notifyDataSetChanged();

    }

    @Override
    public void option3Onclick(View v, int p) {
        final PopupWindow popup;
        Activity3model item = (Activity3model) listData.get(p);
        String alphabet3 = item.getAlphabet3();
        String optiontag3 = item.getOptiontag3();
        final int check3 = item.getCheck3();
        int image3 = item.getOptionimage3();
        int questionimage1 = item.getQuestionimage1();
        int questionimage2 = item.getQuestionimage2();

        int questionimage3 = item.getQuestionimage3();

        String box1 = item.getBox1();
        String box2 = item.getBox2();
        String box3 = item.getBox3();

        Random r = new Random();
        int adv = r.nextInt(10 - 1) + 1;

        if(adv == 8 || adv == 5 || adv == 3){
            InterstitialAd mInterstitialAd;
            final InterstitialAd interstitial;

            AdRequest adRequest = new AdRequest.Builder().build();

            // Prepare the Interstitial Ad
            interstitial = new InterstitialAd(Activity3.this);
// Insert the Ad Unit ID
            interstitial.setAdUnitId(getString(R.string.admob_interstitial_id));

            interstitial.loadAd(adRequest);
// Prepare an Interstitial Ad Listener
            interstitial.setAdListener(new AdListener() {
                public void onAdLoaded() {
// Call displayInterstitial() function
                    // If Ads are loaded, show Interstitial else show nothing.
                    if (interstitial.isLoaded()) {
                        interstitial.show();
                    }
                }
            });
        }


        if(alphabet3 == optiontag3){
            LayoutInflater li = (LayoutInflater)getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
            View layout = li.inflate(R.layout.custom_toast3, (ViewGroup) findViewById(R.id.custom_toast3));

            TextView text = (TextView) layout.findViewById(R.id.question_text) ;
            text.setText(alphabet3);

            ImageView iv = (ImageView) layout.findViewById(R.id.question_image3);

            ImageView iv1 = (ImageView) layout.findViewById(R.id.question_image1);


            ImageView iv2 = (ImageView) layout.findViewById(R.id.question_image2);


            if(box1 == "v" && box2== "v" &&
                    box3 == "i"){

                iv.setImageResource(image3);
                iv1.setImageResource(questionimage1);
                iv2.setImageResource(questionimage2);
            }

            else if(box1 == "v" && box2== "i" &&
                    box3 == "v"){

                iv1.setImageResource(questionimage1);
                iv2.setImageResource(image3);
                iv.setImageResource(questionimage3);

            }

            else if(box1 == "i" && box2== "v" &&
                    box3 == "v"){

                iv1.setImageResource(image3);
                iv2.setImageResource(questionimage2);
                iv.setImageResource(questionimage3);

            }



            popup = new PopupWindow(layout, RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.MATCH_PARENT,true);
            popup.setAnimationStyle(R.style.Animation);
            popup.showAtLocation(layout, Gravity.CENTER, 0, 0);


            new CountDownTimer(2000, 1000) {

                @Override
                public void onTick(long millisUntilFinished) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onFinish() {
                    // TODO Auto-generated method stub

                    popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                        @Override
                        public void onDismiss() {
                            popup.setAnimationStyle(R.style.Animation);
                        }
                    });
                    popup.dismiss();
                    recyclerView.scrollToPosition(check3);
                }
            }.start();
        }
        else{
            LayoutInflater li = getLayoutInflater();
            View layout = li.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.customtos));
            Toast toast = new Toast(getApplicationContext());
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setView(layout);//setting the view of custom toast layout
            toast.show();
        }
//        aAdapter.setListData(listData);
//        aAdapter.notifyDataSetChanged();

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void fab3Onclick(View v, int p) {
        Activity3model item = (Activity3model) listData.get(p);
        int check3 = item.getCheck3();

        Random r = new Random();
        int adv = r.nextInt(10 - 1) + 1;

        if(adv == 8 || adv == 5 || adv == 3){
            InterstitialAd mInterstitialAd;
            final InterstitialAd interstitial;

            AdRequest adRequest = new AdRequest.Builder().build();

            // Prepare the Interstitial Ad
            interstitial = new InterstitialAd(Activity3.this);
// Insert the Ad Unit ID
            interstitial.setAdUnitId(getString(R.string.admob_interstitial_id));

            interstitial.loadAd(adRequest);
// Prepare an Interstitial Ad Listener
            interstitial.setAdListener(new AdListener() {
                public void onAdLoaded() {
// Call displayInterstitial() function
                    // If Ads are loaded, show Interstitial else show nothing.
                    if (interstitial.isLoaded()) {
                        interstitial.show();
                    }
                }
            });
        }


        if(check3== 1){
            finishAffinity();
            Intent intent = new Intent(Activity3.this, Home.class);
            startActivity(intent);
        } else{
            recyclerView.scrollToPosition(check3 - 2);
        }
        aAdapter.setListData(listData);
        aAdapter.notifyDataSetChanged();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void homebuttonclick(int p) {

        Random r = new Random();
        int adv = r.nextInt(10 - 1) + 1;

        if(adv == 8 || adv == 5 || adv == 3){
            InterstitialAd mInterstitialAd;
            final InterstitialAd interstitial;

            AdRequest adRequest = new AdRequest.Builder().build();

            // Prepare the Interstitial Ad
            interstitial = new InterstitialAd(Activity3.this);
// Insert the Ad Unit ID
            interstitial.setAdUnitId(getString(R.string.admob_interstitial_id));

            interstitial.loadAd(adRequest);
// Prepare an Interstitial Ad Listener
            interstitial.setAdListener(new AdListener() {
                public void onAdLoaded() {
// Call displayInterstitial() function
                    // If Ads are loaded, show Interstitial else show nothing.
                    if (interstitial.isLoaded()) {
                        interstitial.show();
                    }
                }
            });
        }
        finishAffinity();
        Intent intent = new Intent (Activity3.this,Home.class);
        startActivity(intent);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void backlinkbutton(int p) {
        Activity3model item = (Activity3model) listData.get(p);
        int check3 = item.getCheck3();
        if(check3== 1){
            finishAffinity();
            Intent intent = new Intent(Activity3.this, Home.class);
            startActivity(intent);
        } else{
            recyclerView.scrollToPosition(check3 - 2);
        }
        aAdapter.setListData(listData);
        aAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main2, menu);
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_home) {
            finishAffinity();
            Intent intent = new Intent (Activity3.this,Home.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Log.i(TAG, "Setting screen name: " + name);
        mTracker.setScreenName("Image~" + name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Share")
                .build());

    }
}
