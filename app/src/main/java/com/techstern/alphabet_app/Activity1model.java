package com.techstern.alphabet_app;

/**
 * Created by sonu on 11/9/16.
 */

public class Activity1model {
    private String alphabet;
    private String spelling;
    private int image1;
    private int image2;
    private int image3;
    private  int check;
    private String tag;
    private String tag1;
    private String tag2;

    public String getSpelling() {
        return spelling;
    }

    public void setSpelling(String spelling) {
        this.spelling = spelling;
    }

    public String getTag1() {
        return tag1;
    }

    public void setTag1(String tag1) {
        this.tag1 = tag1;
    }

    public String getTag2() {
        return tag2;
    }

    public void setTag2(String tag2) {
        this.tag2 = tag2;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getCheck() {
        return check;
    }

    public void setCheck(int check) {
        this.check = check;
    }

    public Activity1model(String alphabet, int image1, int image2, int image3, int check, String tag, String tag1, String tag2,
                          String spelling) {
        this.alphabet = alphabet;
        this.image1 = image1;
        this.image2 = image2;
        this.image3 = image3;
        this.check = check;
        this.tag = tag;
        this.tag1 = tag1;
        this.tag2 = tag2;
        this.spelling = spelling;
    }

    public String getAlphabet() {
        return alphabet;
    }

    public void setAlphabet(String alphabet) {
        this.alphabet = alphabet;
    }

    public int getImage1() {
        return image1;
    }

    public void setImage1(int image1) {
        this.image1 = image1;
    }

    public int getImage2() {
        return image2;
    }

    public void setImage2(int image2) {
        this.image2 = image2;
    }

    public int getImage3() {
        return image3;
    }

    public void setImage3(int image3) {
        this.image3 = image3;
    }
}
