package com.techstern.alphabet_app;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sonu on 11/14/16.
 */

public class Seeddata3 {

    private static final String[] alphabet3 = new String[] {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
            "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};


    private static final int[] question_image1= new int[] {
            R.drawable.apple,
            R.drawable.ball,
            R.drawable.cat,
            R.drawable.dog,
            R.drawable.elephant,
            R.drawable.fish,//6
            R.drawable.goat,
            R.drawable.hat,
            R.drawable.icecream,
            R.drawable.jug,//10
            R.drawable.kite,
            R.drawable.lion,
            R.drawable.monkey,//13
            R.drawable.nest,
            R.drawable.orange,
            R.drawable.parrot,
            R.drawable.queen,
            R.drawable.rabbit,
            R.drawable.sun,
            R.drawable.tiger,//20
            R.drawable.umbrella,
            R.drawable.van,
            R.drawable.well, // not present
            R.drawable.xray,
            R.drawable.yalk,
            R.drawable.zebra

    };

    private static final int[] question_image2= new int[] {
            R.drawable.axe,
            R.drawable.bag,
            R.drawable.cock,
            R.drawable.duck,
            R.drawable.eagle,
            R.drawable.fan,//6
            R.drawable.gun,
            R.drawable.hut,
            R.drawable.inkpot,
            R.drawable.jacket,//10
            R.drawable.key,
            R.drawable.leaf,
            R.drawable.moon,//13
            R.drawable.nose,
            R.drawable.onion,
            R.drawable.pea,
            R.drawable.queenbee,
            R.drawable.robot,
            R.drawable.snake,
            R.drawable.telephone,//20
            R.drawable.urn,
            R.drawable.vase,
            R.drawable.wagon, // not present
            R.drawable.xylophone,
            R.drawable.yolk,
            R.drawable.zip

    };

    private static final int[] question_image3= new int[] {
            R.drawable.aeroplane,
            R.drawable.butterfly,
            R.drawable.cup,
            R.drawable.dolphin,
            R.drawable.egg,
            R.drawable.frog,//6
            R.drawable.grappes,
            R.drawable.horse,
            R.drawable.iron,
            R.drawable.jackal,//10
            R.drawable.king,
            R.drawable.lizard,
            R.drawable.mango,//13
            R.drawable.net,
            R.drawable.owl,
            R.drawable.peacock,
            R.drawable.quail,
            R.drawable.rat,
            R.drawable.squirrel,
            R.drawable.tree,//20
            R.drawable.umbrellabird,
            R.drawable.violin,
            R.drawable.wallet, // not present
            R.drawable.xmas,
            R.drawable.yogurt,
            R.drawable.zoo

    };

    private static final int[] optionimage1= new int[] {
            R.drawable.butterfly,
            R.drawable.mango,
            R.drawable.rat,
            R.drawable.dolphin,
            R.drawable.zoo,
            R.drawable.yogurt,//6
            R.drawable.quail,
            R.drawable.horse,
            R.drawable.tree,
            R.drawable.jackal,//10
            R.drawable.egg,
            R.drawable.iron,
            R.drawable.quail,//13
            R.drawable.aeroplane,
            R.drawable.zoo,
            R.drawable.rat,
            R.drawable.queenbee,
            R.drawable.rat,
            R.drawable.dolphin,
            R.drawable.iron,//20
            R.drawable.grappes,
            R.drawable.van,
            R.drawable.net,
            R.drawable.xmas,
            R.drawable.cup,
            R.drawable.zebra

    };

    private static final int[] optionimage2= new int[] {
            R.drawable.aeroplane,
            R.drawable.owl,
            R.drawable.cat,
            R.drawable.egg,
            R.drawable.owl,
            R.drawable.fan,
            R.drawable.gun,
            R.drawable.wallet, //not present
            R.drawable.horse,
            R.drawable.butterfly,
            R.drawable.squirrel,
            R.drawable.horse,
            R.drawable.mango,//13
            R.drawable.nose,
            R.drawable.egg,
            R.drawable.parrot,
            R.drawable.lizard,
            R.drawable.squirrel,
            R.drawable.urn,
            R.drawable.king,//20
            R.drawable.urn,
            R.drawable.wallet, //not present
            R.drawable.wallet, // not present
            R.drawable.frog,
            R.drawable.yolk,
            R.drawable.mango


    };

    private static final int[] optionimage3 =  new int[] {

            R.drawable.iron,
            R.drawable.bag,
            R.drawable.king,
            R.drawable.frog,
            R.drawable.elephant,
            R.drawable.lizard,
            R.drawable.squirrel,
            R.drawable.xmas,
            R.drawable.icecream,
            R.drawable.dolphin,
            R.drawable.key,
            R.drawable.lion,
            R.drawable.peacock,//13
            R.drawable.butterfly,
            R.drawable.orange,
            R.drawable.yogurt,
            R.drawable.iron,
            R.drawable.tree,
            R.drawable.sun,
            R.drawable.telephone,//20
            R.drawable.frog,
            R.drawable.rat,
            R.drawable.jackal,
            R.drawable.horse,
            R.drawable.dolphin,
            R.drawable.peacock

    };

    private static final String[] optiontag1 = new String[] {"B", "M", "R", "D", "Z", "Y", "Q", "H", "T", "J", "E", "I", "Q", "A", "Z", "R", "Q",
            "R", "D", "I", "G", "V", "N", "X", "C", "Z"};
    private static final String[] optiontag2 = new String[] {"A", "O", "C", "E", "O", "F", "G", "W", "H", "B", "S", "H", "M", "N", "E", "P", "L", "S", "U", "K",
            "U", "W", "W", "F", "Y", "M"};


    private static final String[] optiontag3 = new String[] { "I", "B", "K", "F", "E", "L", "S", "X", "I", "D", "K", "L", "P", "B", "O", "Y", "I", "T", "S",
            "T", "F", "R", "J", "H", "D", "P"};

    private static final int[] check3 = new int[] {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26};

    private static final String[] spelling3 = new String[] {
            "Apple",
            "Ball",
            "Cat",
            "Dog",
            "Elephant",
            "Fish",
            "Goat",
            "Hat",
            "Icecream",
            "Jug",
            "Kite",
            "Lion",
            "Monkey",
            "Nest",
            "Orange",
            "Parrot",
            "Queen",
            "Rabbit",
            "Sun",
            "Tiger",
            "Umbrella",
            "Van",
            "Window", //not available
            "Xray",
            "Yalk",
            "Zebra"
    };

    private static final String[] box1 = new String[]{
            //v = visible
            //i = invisible
            "v",
            "v",
            "i",
            "v", //4
            "i",
            "v",
            "v",
            "v", //8
            "i",
            "v",
            "v",
            "i", //12
            "v",
            "v",
            "i",
            "i", //16
            "v",
            "v",
            "i",
            "v", //20
            "v",
            "i",
            "v",
            "v", //24
            "v",
            "i" //26
    };

    private static final String[] box2 = new String[]{
            //v = visible
            //i = invisible
            "v",
            "i",
            "v",
            "v", //4
            "v",
            "i",
            "i",
            "v", //8
            "v",
            "v",
            "i",
            "v", //12
            "v",
            "i",
            "v",
            "v", //16
            "i",
            "v",
            "v",
            "i", //20
            "i",
            "v",
            "v",
            "v", //24
            "i",
            "v" //26
    };

    private static final String[] box3 = new String[]{
            //v = visible
            //i = invisible
            "i",
            "v",
            "v",
            "i", //4
            "v",
            "v",
            "v",
            "i", //8
            "v",
            "i",
            "v",
            "v", //12
            "i",
            "v",
            "v",
            "v", //16
            "v",
            "i",
            "v",
            "v", //20
            "v",
            "v",
            "i",
            "i", //24
            "v",
            "v" //26
    };


    public static List<Activity3model> getListData(){
        List<Activity3model> data = new ArrayList<>();
        for (int i = 0; i < alphabet3.length; i++)
        {

            Activity3model ac3m = new Activity3model(alphabet3[i], question_image1[i],
                    question_image2[i], optionimage1[i], optionimage2[i], optionimage3[i], optiontag1[i], optiontag2[i],
                    optiontag3[i], check3[i], spelling3[i], box1[i], box2[i], box3[i], question_image3[i]);
            // Binds all strings into an array
            data.add(ac3m);

        }
        return data;
    }


}



