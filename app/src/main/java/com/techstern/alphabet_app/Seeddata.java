package com.techstern.alphabet_app;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sonu on 11/11/16.
 */

public class Seeddata {

    private static final String[] alphabet = new String[] {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
            "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

    private static final String[] tag = new String[] {"B", "M", "R", "D", "Z", "Y", "Q", "H", "T", "J", "E", "I", "Q", "A", "Z", "R", "Q",
            "R", "D", "I", "G", "V", "N", "X", "C", "Z"};
    private static final String[] tag1 = new String[] {"A", "O", "C", "E", "O", "F", "G", "W", "H", "B", "S", "H", "M", "N", "E", "P", "L", "S", "U", "K",
            "U", "W", "W", "F", "Y", "M"};


    private static final String[] tag2 = new String[] { "I", "B", "K", "F", "E", "L", "S", "X", "I", "D", "K", "L", "P", "B", "O", "Y", "I", "T", "S",
            "T", "F", "R", "J", "H", "D", "P"};
    private static final int[] image1= new int[] {
            R.drawable.ball,
            R.drawable.monkey,
            R.drawable.rabbit,
            R.drawable.dog,
            R.drawable.zebra,
            R.drawable.yalk,//6
            R.drawable.queen,
            R.drawable.hat,
            R.drawable.tiger,
            R.drawable.jug,//10
            R.drawable.elephant,
            R.drawable.icecream,
            R.drawable.queen,//13
            R.drawable.apple,
            R.drawable.zebra,
            R.drawable.rabbit,
            R.drawable.queen,
            R.drawable.rabbit,
            R.drawable.dog,
            R.drawable.icecream,//20
            R.drawable.goat,
            R.drawable.van,
            R.drawable.nest,
            R.drawable.xray,
            R.drawable.cat,
            R.drawable.zebra

    };

    private static final int[] image2 = new int[] {
            R.drawable.apple,
            R.drawable.orange,
            R.drawable.cat,
            R.drawable.elephant,
            R.drawable.orange,
            R.drawable.fish,
            R.drawable.goat,
            R.drawable.well,   //not present
            R.drawable.hat,
            R.drawable.ball,
            R.drawable.sun,
            R.drawable.hat,
            R.drawable.monkey,//13
            R.drawable.nest,
            R.drawable.elephant,
            R.drawable.parrot,
            R.drawable.lion,
            R.drawable.sun,
            R.drawable.umbrella,
            R.drawable.kite,//20
            R.drawable.umbrella,
            R.drawable.well,   //not present
            R.drawable.well,   //not present
            R.drawable.fish,
            R.drawable.yalk,
            R.drawable.monkey


    };
    private static final int[] image3 =  new int[] {

            R.drawable.icecream,
            R.drawable.ball,
            R.drawable.kite,
            R.drawable.fish,
            R.drawable.elephant,
            R.drawable.lion,
            R.drawable.sun,
            R.drawable.xray,
            R.drawable.icecream,
            R.drawable.dog,
            R.drawable.kite,
            R.drawable.lion,
            R.drawable.parrot,//13
            R.drawable.ball,
            R.drawable.orange,
            R.drawable.yalk,
            R.drawable.icecream,
            R.drawable.tiger,
            R.drawable.sun,
            R.drawable.tiger,//20
            R.drawable.fish,
            R.drawable.rabbit,
            R.drawable.jug,
            R.drawable.hat,
            R.drawable.dog,
            R.drawable.parrot

    };
    private static final int[] check = new int[] {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26};
    private static final String[] spelling = new String[] {
            "Apple",
             "Ball",
            "Cat",
            "Dog",
            "Elephant",
            "Fish",
            "Goat",
            "Hat",
            "Icecream",
            "Jug",
            "Kite",
            "Lion",
            "Monkey",
            "Nest",
            "Orange",
            "Parrot",
            "Queen",
            "Rabbit",
            "Sun",
            "Tiger",
            "Umbrella",
            "Van",
            "Well",
            "Xray",
            "Yalk",
            "Zebra"
    };

    public static List<Activity1model> getListData(){
        List<Activity1model> data = new ArrayList<>();
        for (int i = 0; i < alphabet.length; i++)
        {

            Activity1model ac1m = new Activity1model(alphabet[i], image1[i],
                    image2[i], image3[i], check[i], tag[i],tag1[i], tag2[i], spelling[i]);
            // Binds all strings into an array
            data.add(ac1m);

        }
        return data;
    }
}
