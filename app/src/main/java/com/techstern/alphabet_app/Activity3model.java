package com.techstern.alphabet_app;

/**
 * Created by sonu on 11/14/16.
 */

public class Activity3model {

    private  String alphabet3;
    private int questionimage1;
    private int questionimage2;
    private int questionimage3;
    private int optionimage1;
    private int optionimage2;
    private int optionimage3;

    private String optiontag1;
    private String optiontag2;
    private String optiontag3;
    private String spelling3;
    private String box1;
    private String box2;
    private String box3;



    private int check3;


    public Activity3model(String alphabet3, int questionimage1, int questionimage2, int optionimage1,
                          int optionimage2, int optionimage3, String optiontag1, String optiontag2,
                          String optiontag3, int check3, String spelling3, String box1, String box2, String box3, int questionimage3) {
        this.alphabet3 = alphabet3;
        this.questionimage1 = questionimage1;
        this.questionimage2 = questionimage2;
        this.questionimage3 = questionimage3;
        this.optionimage1 = optionimage1;
        this.optionimage2 = optionimage2;
        this.optionimage3 = optionimage3;
        this.optiontag1 = optiontag1;
        this.optiontag2 = optiontag2;
        this.optiontag3 = optiontag3;
        this.check3 = check3;
        this.spelling3 = spelling3;
        this.box1 = box1;
        this.box2 = box2;
        this.box3 = box3;

    }

    public int getQuestionimage3() {
        return questionimage3;
    }

    public void setQuestionimage3(int questionimage3) {
        this.questionimage3 = questionimage3;
    }

    public String getBox1() {
        return box1;
    }

    public void setBox1(String box1) {
        this.box1 = box1;
    }

    public String getBox2() {
        return box2;
    }

    public void setBox2(String box2) {
        this.box2 = box2;
    }

    public String getBox3() {
        return box3;
    }

    public void setBox3(String box3) {
        this.box3 = box3;
    }

    public String getSpelling3() {
        return spelling3;
    }

    public void setSpelling3(String spelling3) {
        this.spelling3 = spelling3;
    }

    public String getAlphabet3() {
        return alphabet3;
    }

    public void setAlphabet3(String alphabet3) {
        this.alphabet3 = alphabet3;
    }

    public int getCheck3() {
        return check3;
    }

    public void setCheck3(int check3) {
        this.check3 = check3;
    }



    public int getQuestionimage1() {
        return questionimage1;
    }

    public void setQuestionimage1(int questionimage1) {
        this.questionimage1 = questionimage1;
    }

    public int getQuestionimage2() {
        return questionimage2;
    }

    public void setQuestionimage2(int questionimage2) {
        this.questionimage2 = questionimage2;
    }

    public int getOptionimage1() {
        return optionimage1;
    }

    public void setOptionimage1(int optionimage1) {
        this.optionimage1 = optionimage1;
    }

    public int getOptionimage2() {
        return optionimage2;
    }

    public void setOptionimage2(int optionimage2) {
        this.optionimage2 = optionimage2;
    }

    public int getOptionimage3() {
        return optionimage3;
    }

    public void setOptionimage3(int optionimage3) {
        this.optionimage3 = optionimage3;
    }

    public String getOptiontag1() {
        return optiontag1;
    }

    public void setOptiontag1(String optiontag1) {
        this.optiontag1 = optiontag1;
    }

    public String getOptiontag2() {
        return optiontag2;
    }

    public void setOptiontag2(String optiontag2) {
        this.optiontag2 = optiontag2;
    }

    public String getOptiontag3() {
        return optiontag3;
    }

    public void setOptiontag3(String optiontag3) {
        this.optiontag3 = optiontag3;
    }
}
