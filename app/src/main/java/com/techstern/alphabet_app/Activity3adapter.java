package com.techstern.alphabet_app;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by sonu on 11/14/16.
 */

public class Activity3adapter extends RecyclerView.Adapter<Activity3adapter.MyViewHolder>{

    private List<Activity3model> activity3modelList;
    private ItemClickCallBack itemClickCallBack;

    public interface ItemClickCallBack {
        void option1Onclick(View v, int p);
        void option2Onclick(View v, int p);
        void option3Onclick(View v, int p);
//        void option4Onclick(View v, int p);
        void fab3Onclick(View v, int p);
        void homebuttonclick(int p);
        void backlinkbutton(int p);
    }

    public void setItemClickCallBack(final ItemClickCallBack itemClickCallBack){
        this.itemClickCallBack = itemClickCallBack;
    }



    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView question_text;
        public ImageView question_image1, question_image2,question_image3, option_image1, option_image2,
        option_image3, fab3, backlink3,homebutton3;

//        public ImageView img1,img2,img3,floting;

        public MyViewHolder(View view) {
            super(view);
            question_text = (TextView) view.findViewById(R.id.question_text);
            question_image1 = (ImageView) view.findViewById(R.id.question_image1);
            question_image2 = (ImageView) view.findViewById(R.id.question_image2);
            question_image3 = (ImageView) view.findViewById(R.id.question_image3);
            option_image1 = (ImageView) view.findViewById(R.id.option_image_1);
            option_image2 = (ImageView) view.findViewById(R.id.option_image_2);
            option_image3 = (ImageView) view.findViewById(R.id.option_image_3);
            fab3= (ImageView) view.findViewById(R.id.fab3) ;
            backlink3 = (ImageView) view.findViewById(R.id.backclick3);
            homebutton3 = (ImageView) view.findViewById(R.id.homebutton3);

             option_image1.setOnClickListener(this);
             option_image2.setOnClickListener(this);
             option_image3.setOnClickListener(this);
             fab3.setOnClickListener(this);
            backlink3.setOnClickListener(this);
            homebutton3.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if(v.getId() == R.id.option_image_1){
                Activity3model activity3model = activity3modelList.get(getAdapterPosition());
                itemClickCallBack.option1Onclick(v, getAdapterPosition());



                if(activity3model.getBox1() == "v" && activity3model.getBox2()== "v" &&
                        activity3model.getBox3() == "i"){

                    question_image3.setImageResource(activity3model.getOptionimage1());
                }

                else if(activity3model.getBox1() == "v" && activity3model.getBox2()== "i" &&
                        activity3model.getBox3() == "v"){

                    question_image2.setImageResource(activity3model.getOptionimage1());
                }

                else if(activity3model.getBox1() == "i" && activity3model.getBox2()== "v" &&
                        activity3model.getBox3() == "v"){

                    question_image1.setImageResource(activity3model.getOptionimage1());

                }






            }
            else if(v.getId() == R.id.option_image_2) {
                Activity3model activity3model = activity3modelList.get(getAdapterPosition());
                itemClickCallBack.option2Onclick(v, getAdapterPosition());
                if(activity3model.getBox1() == "v" && activity3model.getBox2()== "v" &&
                        activity3model.getBox3() == "i"){

                    question_image3.setImageResource(activity3model.getOptionimage2());
                }

                else if(activity3model.getBox1() == "v" && activity3model.getBox2()== "i" &&
                        activity3model.getBox3() == "v"){

                    question_image2.setImageResource(activity3model.getOptionimage2());
                }

                else if(activity3model.getBox1() == "i" && activity3model.getBox2()== "v" &&
                        activity3model.getBox3() == "v"){

                    question_image1.setImageResource(activity3model.getOptionimage2());

                }
            }
            else if(v.getId() == R.id.option_image_3) {
                Activity3model activity3model = activity3modelList.get(getAdapterPosition());
                itemClickCallBack.option3Onclick(v, getAdapterPosition());
                if(activity3model.getBox1() == "v" && activity3model.getBox2()== "v" &&
                        activity3model.getBox3() == "i"){

                    question_image3.setImageResource(activity3model.getOptionimage3());
                }

                else if(activity3model.getBox1() == "v" && activity3model.getBox2()== "i" &&
                        activity3model.getBox3() == "v"){

                    question_image2.setImageResource(activity3model.getOptionimage3());
                }

                else if(activity3model.getBox1() == "i" && activity3model.getBox2()== "v" &&
                        activity3model.getBox3() == "v"){

                    question_image1.setImageResource(activity3model.getOptionimage3());

                }
            }

            else if (v.getId() == R.id.fab3){
                itemClickCallBack.fab3Onclick(v, getAdapterPosition());
            }
            else if (v.getId() == R.id.backclick3){
                itemClickCallBack.backlinkbutton(getAdapterPosition());
            }
            else if (v.getId() == R.id.homebutton3){
                itemClickCallBack.homebuttonclick(getAdapterPosition());
            }
        }
    }


    public Activity3adapter(List<Activity3model> activity3modelList) {
        this.activity3modelList = activity3modelList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.itemstyleactivity3, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Activity3model activity3model = activity3modelList.get(position);

        holder.question_text.setText(activity3model.getAlphabet3());

        holder.option_image1.setImageResource(activity3model.getOptionimage1());
        holder.option_image2.setImageResource(activity3model.getOptionimage2());
        holder.option_image3.setImageResource(activity3model.getOptionimage3());


        if(activity3model.getBox1() == "v" && activity3model.getBox2()== "v" &&
                activity3model.getBox3() == "i"){

            holder.question_image1.setImageResource(activity3model.getQuestionimage1());
            holder.question_image2.setImageResource(activity3model.getQuestionimage2());
            holder.question_image3.setImageDrawable(null);

        }

        else if(activity3model.getBox1() == "v" && activity3model.getBox2()== "i" &&
                activity3model.getBox3() == "v"){

            holder.question_image1.setImageResource(activity3model.getQuestionimage1());
            holder.question_image3.setImageResource(activity3model.getQuestionimage3());
            holder.question_image2.setImageDrawable(null);
        }

        else if(activity3model.getBox1() == "i" && activity3model.getBox2()== "v" &&
                activity3model.getBox3() == "v"){

            holder.question_image2.setImageResource(activity3model.getQuestionimage2());
            holder.question_image3.setImageResource(activity3model.getQuestionimage3());
            holder.question_image1.setImageDrawable(null);

        }


    }

    public void setListData(List<Activity3model> exerciseList) {
        this.activity3modelList.clear();

        this.activity3modelList.addAll(exerciseList);
    }

    @Override
    public int getItemCount() {
        return activity3modelList.size();
    }
}
