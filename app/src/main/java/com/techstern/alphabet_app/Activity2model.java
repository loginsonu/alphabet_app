package com.techstern.alphabet_app;

/**
 * Created by sonu on 11/12/16.
 */

public class Activity2model {
    private  String question1;
    private String question2;
    private String answer;
    private String question1visibility;
    private String question2visibility;
    private String answervisibility;
    private String answercheck;
    private String option1;
    private String option2;
    private String option3;
    private String option4;
    private int check2;


    public Activity2model(String question1, String question2, String answer, String option1,
                          String option2, String option3, String option4, int check2, String question1visibility,
                          String question2visibility, String answervisibility, String answercheck) {
        this.question1 = question1;
        this.question2 = question2;
        this.answer = answer;
        this.option1 = option1;
        this.option2 = option2;
        this.option3 = option3;
        this.option4 = option4;
        this.check2 = check2;
        this.question1visibility = question1visibility;
        this.question2visibility = question2visibility;
        this.answervisibility = answervisibility;
        this.answercheck = answercheck;

    }

    public String getAnswercheck() {
        return answercheck;
    }

    public void setAnswercheck(String answercheck) {
        this.answercheck = answercheck;
    }

    public String getQuestion1visibility() {
        return question1visibility;
    }

    public void setQuestion1visibility(String question1visibility) {
        this.question1visibility = question1visibility;
    }

    public String getQuestion2visibility() {
        return question2visibility;
    }

    public void setQuestion2visibility(String question2visibility) {
        this.question2visibility = question2visibility;
    }

    public String getAnswervisibility() {
        return answervisibility;
    }

    public void setAnswervisibility(String answervisibility) {
        this.answervisibility = answervisibility;
    }

    public int getCheck2() {
        return check2;
    }

    public void setCheck2(int check2) {
        this.check2 = check2;
    }

    public String getQuestion1() {
        return question1;
    }

    public void setQuestion1(String question1) {
        this.question1 = question1;
    }

    public String getQuestion2() {
        return question2;
    }

    public void setQuestion2(String question2) {
        this.question2 = question2;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getOption1() {
        return option1;
    }

    public void setOption1(String option1) {
        this.option1 = option1;
    }

    public String getOption2() {
        return option2;
    }

    public void setOption2(String option2) {
        this.option2 = option2;
    }

    public String getOption3() {
        return option3;
    }

    public void setOption3(String option3) {
        this.option3 = option3;
    }

    public String getOption4() {
        return option4;
    }

    public void setOption4(String option4) {
        this.option4 = option4;
    }
}
